import unittest
from app import *

class TestsWhatsYourName(unittest.TestCase):
    
    
    def testFirstLetter(self):
        self.assertEqual(firstLetter('Nicolas'),'N')
        self.assertEqual(firstLetter('abcd'),'A')
        self.assertEqual(firstLetter('0'),'error')
        self.assertEqual(firstLetter(''),'error')

    def testGiveMeMyNickname(self):
        self.assertEqual(giveMeMyNickname('Nicolas','Davenne','mechant'),'The Crazy Skull')
        self.assertEqual(giveMeMyNickname('Abc','efg',"mechant"),'The Evil Mastermind')
        self.assertEqual(giveMeMyNickname('','Davenne','mechant'),'error Skull')