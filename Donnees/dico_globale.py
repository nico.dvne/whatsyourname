from Donnees import indian
from Donnees import mechant
from Donnees import super_heros

donnees_prenom = {
    'indien' : indian.indian_firstname,
    'superheros' : super_heros.super_heros_firstname,
    'mechant' : mechant.mechant_firstname
}

donnees_nom = {
    'indien' : indian.indian_lastname,
    'superheros' : super_heros.super_heros_lastname,
    'mechant' : mechant.mechant_lastname
}