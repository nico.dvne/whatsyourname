mechant_firstname = {
    'A' : 'The Evil',
    'B' : 'The Mad',
    'C' : 'The Big',
    'D' : 'The Dangerous',
    'E' : 'Captain',
    'F' : 'The Ghostly',
    'G' : 'Professor',
    'H' : 'Doctor',
    'I' : 'Phantom',
    'J' : 'The Brutal',
    'K' : 'The Unstoppable',
    'L' : 'The Vile',
    'M' : 'The Dark',
    'N' : 'The Crazy',
    'O' : 'The Iron',
    'P' : 'The Poison',
    'Q' : 'The Brutal',
    'R' : 'The Bloody',
    'S' : 'The Dark',
    'T' : 'The Dangerous',
    'U' : 'The Rancid',
    'V' : 'The Invisible',
    'W' : 'The Black',
    'X' : 'The Atomic',
    'Y' : 'The Mega',
    'Z' : 'The Grand',
    'error': 'error'
}

mechant_lastname = {
    'A' : 'Shadow',
    'B' : 'Knight',
    'C' : 'Tarantula',
    'D' : 'Skull',
    'E' : 'Mastermind',
    'F' : 'Wizard',
    'G' : 'Ninja',
    'H' : 'Devil',
    'I' : 'Freak',
    'J' : 'Beast',
    'K' : 'Criminal',
    'L' : 'Master',
    'M' : 'Lord',
    'N' : 'Child',
    'O' : 'Corpse',
    'P' : 'Slayer',
    'Q' : 'Spider',
    'R' : 'Creature',
    'S' : 'Werewolf',
    'T' : 'Monster',
    'U' : 'Vampire',
    'V' : 'Mutant',
    'W' : 'Robot',
    'X' : 'Claw',
    'Y' : 'Machine',
    'Z' : 'Clown',
    'error': 'error'
}
