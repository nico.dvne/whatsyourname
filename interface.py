from tkinter import *
from tkinter.messagebox import *
import tkinter.font as tkFont

from app import *

root = Tk()
root.title("Le chiffre de Cesar")

firstname = StringVar()
firstname.set("Entrez votre prenom")

lastname = StringVar()
lastname.set("Entrez votre nom de famille")

text_indian = 'indien'
text_superH = 'superheros'
text_mechant = 'mechant'

def button_indian_callback():
    retour.set(giveMeMyNickname(firstname.get(),lastname.get(),text_indian))

def button_superheros_callback():
    retour.set(giveMeMyNickname(firstname.get(),lastname.get(),text_superH))

def button_mechant_callback():
    retour.set(giveMeMyNickname(firstname.get(),lastname.get(),text_mechant))


input_firstname = Entry(root, textvariable=firstname)
input_firstname.grid(column=1, row=0,pady=15,padx=15)
input_firstname.config( width = 40) 

input_lastname = Entry(root, textvariable=lastname)
input_lastname.grid(column=3, row=0,pady=15,padx=15)
input_lastname.config(width = 40)

retour = StringVar()

output_resultat = Entry(root, textvariable = retour)
output_resultat.grid(column = 2, row=1,pady=15,padx=15) 
output_resultat.config(width = 40)



button_indian = Button(root,text = text_indian ,command=button_indian_callback)
button_indian.grid(row = 2,column = 1,padx = 15,pady = 15)
button_indian.config(height = 2, bg = 'orange', fg = 'black')

button_superheros = Button(root,text = text_superH, command=button_superheros_callback)
button_superheros.grid(row = 2,column = 2,padx = 15,pady = 15)
button_superheros.config(height = 2, bg = 'red', fg = 'white')

button_mechant = Button(root,text = text_mechant, command=button_mechant_callback)
button_mechant.grid(row = 2 , column = 3 , padx = 15 , pady = 15)
button_mechant.config(height = 2 , bg = 'blue', fg = 'white')

root.mainloop()
