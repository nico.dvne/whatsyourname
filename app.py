from Donnees import dico_globale
import string

caractere_possible = string.ascii_uppercase

'''
Firstletter prend une chaine de caractere et en retourne la premiere lettre
en majuscule si celle ci est une lettre.  
'''
def firstLetter(chaine_de_cara):
    if not chaine_de_cara:
        return 'error'
    else:
        to_test = chaine_de_cara[0].upper()
        if (not(not to_test)) and to_test in caractere_possible:        
            return to_test
        else:
            to_test = 'error'
            return to_test

   
    


'''
Le but de giveMeMyNickname est de retourner le surnom final
args : prenom,nom,univers
return : le surnom

algo : on prend la premiere lettre du nom et du prenom
        et on va chercher le surnom correspondant
        On concatene
        Et hop
'''
def giveMeMyNickname(firstname,lastname,domaine):
    nickname_firstname = dico_globale.donnees_prenom[domaine][firstLetter(firstname)]
    nickname_lastname = dico_globale.donnees_nom[domaine][firstLetter(lastname)]
    nickname = f"{nickname_firstname} {nickname_lastname}"
    return nickname
